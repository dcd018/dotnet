using System.Collections.Generic;

namespace EWSPushNotifications
{
    public partial class SubscriptionEvent
    {
        public string ItemId = null;

        public List<string> Recipients = null;

        public List<string> CcRecipients = null;
    }
}
