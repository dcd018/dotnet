﻿using System;
using System.Text;
using System.Collections.Generic;

using CommandLine;
using CommandLine.Text;

using Microsoft.Exchange.WebServices.Data;

using System.Net;
using System.Net.Security;
using System.Security.Cryptography.X509Certificates;

using Amazon;

namespace EWSPushNotifications
{
    class Options
    {
        [Option('h', "host", Required = true, HelpText = "Exchange Web Services endpoint.")]
        public string Endpoint { get; set; }

        [Option('m', "mailbox", Required = true, HelpText = "Mailbox of the user to subscribe to push notifications on.")]
        public string Mailbox { get; set; }

        [Option('l', "local", HelpText = "Authenticate with signed in user.")]
        public bool UseDefaultCredentials { get; set; }

        [Option('u', "user", HelpText = "User to authenticate.")]
        public string User { get; set; }

        [Option('P', "password", HelpText = "Password of authenticated user.")]
        public string Password { get; set; }

        [Option('d', "domain", HelpText = "Domain the authenticated user belongs to.")]
        public string Domain { get; set; }

        [Option('a', "listen-port", HelpText = "Push notifications listen port.")]
        public int ListenPort { get; set; }

        [Option('f', "folder", HelpText = "Well Known folder to subscribe to.")]
        public string WellKnownFolder { get; set; }

        [OptionArray('a', "events", Required = true, HelpText = "Selected events to subscribe to.")]
        public string[] SelectedEvents { get; set; }

        [Option('k', "impersonate", HelpText = "Specify whether to impersonate or delegate.")]
        public bool Impersonate { get; set; }

        [Option('q', "lambda-function", Required = true, HelpText = "Lambda function name to dispatch items to.")]
        public string LambdaFunction { get; set; }

        [Option('r', "region", HelpText = "Region of Lambda function")]
        public string Region { get; set; }

        [Option('o', "outlog", HelpText = "File path of stdout log file.")]
        public string InfoLogFile { get; set; }

        [Option('i', "eventlog", HelpText = "File path of events log file.")]
        public string EventsLogFile { get; set; }

        [Option('e', "errlog", HelpText = "File path of stderr log file.")]
        public string ErrorLogFile { get; set; }

        [Option('v', "verbose", HelpText = "Print details during execution.")]
        public bool Verbose { get; set; }

        [Option('w', "disable-logging", DefaultValue = false, HelpText = "Disable writing of logs to file system.")]
        public bool DisableLogging { get; set; }

        [Option('y', "enable-event-logging", DefaultValue = false, HelpText = "Enables Windows event logging.")]
        public bool EnableEventLogging { get; set; }

        [Option('x', "dryrun", HelpText = "Print details without execution.")]
        public bool DryRun { get; set; }

        [HelpOption]
        public string GetUsage()
        {
            HelpText help = new HelpText
            {
                Heading = new HeadingInfo("Exchange Push Notifications", "v0.0.1"),
                AdditionalNewLineAfterOption = true,
                AddDashesToOption = true
            };
            help.AddPreOptionsLine("Usage:");
            help.AddOptions(this);
            return help;
        }
    }
    public partial class Session
    {
        public ExchangeService ExchangeService = null;

        public string Domain = null;

        public string Mailbox = null;

        public string WellKnownFolder = null;

        public string[] SelectedEvents = new string[] { };

        public bool Impersonate = false;

        public string LambdaFunction = null;

        public RegionEndpoint RegionEndpoint = RegionEndpoint.USEast1;

        public string InfoLogFile = "log/stdout.log";

        public string EventsLogFile = "log/events.log";

        public string ErrorLogFile = "log/stderr.log";

        public bool DisableLogging = false;

        public bool EnableEventLogging = false;

        public bool Verbose = false;

        private string Endpoint = null;

        public int ListenPort = 36728;

        private bool UseDefaultCredentials = false;

        private WebCredentials Credentials = null;

        public Session(string[] args)
        {
            Options options = new Options();
            if (CommandLine.Parser.Default.ParseArguments(args, options))
            {
                if (options.Verbose || options.DryRun)
                {
                    Console.WriteLine("-h, --host: " + options.Endpoint);
                    Console.WriteLine("-a, --listen-port: " + options.ListenPort);
                    Console.WriteLine("-m, --mailbox: " + options.Mailbox);
                    if (options.UseDefaultCredentials)
                    {
                        Console.WriteLine("-l, --local: " + options.UseDefaultCredentials);
                    }
                    if (!String.IsNullOrEmpty(options.User))
                    {
                        Console.WriteLine("-u, --user: " + options.User);
                    }
                    if (!String.IsNullOrEmpty(options.Password))
                    {
                        Console.WriteLine("-P, --password: " + options.Password);
                    }
                    if (!String.IsNullOrEmpty(options.Domain))
                    {
                        Console.WriteLine("-d, --domain: " + options.Domain);
                    }
                    if (!String.IsNullOrEmpty(options.WellKnownFolder))
                    {
                        Console.WriteLine("-f, --folder: " + options.WellKnownFolder);
                    }
                    Console.WriteLine("-a, --events: " + String.Join(",", options.SelectedEvents));
                    if (options.Impersonate)
                    {
                        Console.WriteLine("-k, --impersonate: " + options.Impersonate);
                    }
                    Console.WriteLine("-q, --lambda-function: " + options.LambdaFunction);
                    if (!String.IsNullOrEmpty(options.Region))
                    {
                        Console.WriteLine("-r, --region: " + options.Region);
                    }
                    if (!String.IsNullOrEmpty(options.InfoLogFile))
                    {
                        Console.WriteLine("-o, --outlog: " + options.InfoLogFile);
                    }
                    if (!String.IsNullOrEmpty(options.EventsLogFile))
                    {
                        Console.WriteLine("-i, --eventslog: " + options.EventsLogFile);
                    }
                    if (!String.IsNullOrEmpty(options.ErrorLogFile))
                    {
                        Console.WriteLine("-e, --errlog: " + options.ErrorLogFile);
                    }
                    if (options.DisableLogging)
                    {
                        Console.WriteLine("-w, --disable-logging: " + options.DisableLogging);
                    }
                    if (options.EnableEventLogging)
                    {
                        Console.WriteLine("-w, --enable-event-logging: " + options.EnableEventLogging);
                    }
                }

                if (!options.DryRun)
                {
                    SetOptions(options);
                    SetService();
                }
            }
        }

        private void SetOptions(Options options)
        {
            try
            {
                if (options.UseDefaultCredentials)
                {
                    UseDefaultCredentials = true;
                }
                else if (
                  !String.IsNullOrEmpty(options.User)
                  && !String.IsNullOrEmpty(options.Password)
                  && !String.IsNullOrEmpty(options.Domain)
                )
                {
                    Credentials = new WebCredentials(options.User, options.Password, options.Domain);
                }
                else
                {
                    if (String.IsNullOrEmpty(options.User))
                    {
                        throw new System.ArgumentException("Parameter cannot be null when -l; --local is specified", "-u; --user");
                    }
                    else if (String.IsNullOrEmpty(options.Password))
                    {
                        throw new System.ArgumentException("Parameter cannot be null when -l; --local is specified", "-P; --password");
                    }
                    else if (String.IsNullOrEmpty(options.Domain))
                    {
                        throw new System.ArgumentException("Parameter cannot be null when -l; --local is specified", "-d; --domain");
                    }
                }

                if (!String.IsNullOrEmpty(options.Endpoint))
                {
                    Endpoint = options.Endpoint;
                }

                if (options.ListenPort > 0)
                {
                    ListenPort = options.ListenPort;
                }

                if (!String.IsNullOrEmpty(options.Mailbox))
                {
                    Mailbox = options.Mailbox;
                }

                if (!String.IsNullOrEmpty(options.WellKnownFolder))
                {
                    WellKnownFolder = options.WellKnownFolder;
                }

                SelectedEvents = options.SelectedEvents;

                Impersonate = options.Impersonate;

                LambdaFunction = options.LambdaFunction;

                if (!String.IsNullOrEmpty(options.Region))
                {
                    RegionEndpoint = GetRegionEndpoint(options.Region);
                }
                if (!String.IsNullOrEmpty(options.InfoLogFile))
                {
                    InfoLogFile = options.InfoLogFile;
                }
                if (!String.IsNullOrEmpty(options.EventsLogFile))
                {
                    EventsLogFile = options.EventsLogFile;
                }
                if (!String.IsNullOrEmpty(options.ErrorLogFile))
                {
                    ErrorLogFile = options.ErrorLogFile;
                }

                Verbose = options.Verbose;

                DisableLogging = options.DisableLogging;

                EnableEventLogging = options.EnableEventLogging;
            }
            catch (Exception e)
            {
                Console.Out.WriteLine(e);
            }
        }

        private RegionEndpoint GetRegionEndpoint(string region)
        {
            RegionEndpoint regionEndpoint = RegionEndpoint.USEast1;
            switch (region)
            {
                case "ap-northeast-1": { regionEndpoint = RegionEndpoint.APNortheast1; break; }
                case "ap-southeast-1": { regionEndpoint = RegionEndpoint.APSoutheast1; break; }
                case "ap-southeast-2": { regionEndpoint = RegionEndpoint.APSoutheast2; break; }
                case "eu-west-1": { regionEndpoint = RegionEndpoint.EUWest1; break; }
                case "sa-east-1": { regionEndpoint = RegionEndpoint.SAEast1; break; }
                case "us-east-1": { regionEndpoint = RegionEndpoint.USEast1; break; }
                case "us-east-2": { regionEndpoint = RegionEndpoint.USEast2; break; }
                case "us-west-1": { regionEndpoint = RegionEndpoint.USWest1; break; }
                case "us-west-2": { regionEndpoint = RegionEndpoint.USWest2; break; }
            }
            return regionEndpoint;
        }

        private void SetService()
        {
            ExchangeService = new ExchangeService(ExchangeVersion.Exchange2013);

            if (UseDefaultCredentials)
            {
                ExchangeService.UseDefaultCredentials = true;
            }
            else
            {
                ExchangeService.Credentials = Credentials;
            }

            ExchangeService.Url = new Uri(Endpoint);
            ServicePointManager.ServerCertificateValidationCallback = CertificateValidationCallBack;

            if (Impersonate)
            {
                ExchangeService.ImpersonatedUserId = new ImpersonatedUserId(ConnectingIdType.SmtpAddress, Mailbox);
            }
        }

        private static bool CertificateValidationCallBack(
         object sender,
         System.Security.Cryptography.X509Certificates.X509Certificate certificate,
         System.Security.Cryptography.X509Certificates.X509Chain chain,
         System.Net.Security.SslPolicyErrors sslPolicyErrors)
        {
            return true;
        }
    }
}
