using System;
using System.IO;

namespace EWSPushNotifications
{
    public partial class Logger
    {
        public static string InfoLogFile = null;

        public static string EventsLogFile = null;

        public static string ErrorLogFile = null;

        public static bool Verbose = false;

        public static bool DisableLogging = false;

        public static bool EnableEventLogging = false;

        public static void LogMessage(string message, string description = null)
        {
            Log(InfoLogFile, message, description);

            if (EnableEventLogging) {
                string msg = (description != null && (description.Length > 0))
                    ? description + ": " + message
                    : message;

                EventLogger.LogInformation(msg);
            }
        }

        public static void LogEvent(string message, string description = null)
        {
            Log(EventsLogFile, message, description);

            if (EnableEventLogging) {
                string msg = (description != null && (description.Length > 0))
                    ? description + ": " + message
                    : message;

                EventLogger.LogInformation(msg);
            }
        }

        public static void LogError(Exception e, string description = null)
        {
            string err = (Verbose) ? e.ToString() : e.Message;
            Log(ErrorLogFile, err, description);

            if (EnableEventLogging) {
                EventLogger.LogException(e);
            }
        }

        public static void Log(string logFile, string logMessage, string logDescription = null)
        {
            string msg = (logDescription != null && (logDescription.Length > 0))
                ? logDescription + ": " + logMessage
                : logMessage;

            if (!DisableLogging)
            {
                Console.WriteLine(msg);

                try
                {
                    string directory = Path.GetDirectoryName(logFile);

                    if (!String.IsNullOrEmpty(directory) && !Directory.Exists(directory))
                    {
                        Directory.CreateDirectory(directory);
                    }

                    using (StreamWriter w = File.AppendText(logFile))
                    {
                        string line = String.Format("{0} {1}: {2}", DateTime.Now.ToLongTimeString(), DateTime.Now.ToLongDateString(), msg);
                        w.WriteLine(line);
                    }
                }
                catch (Exception e)
                {
                    Console.WriteLine(e);
                }
            }
        }
    }
}