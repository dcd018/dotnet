using System;
using System.IO;
using System.Collections.Generic;
using System.Timers;
using System.Data;
using Microsoft.Exchange.WebServices.Data;
using System.Threading;
using System.Runtime.Serialization.Json;
using System.Text;

using Amazon;
using Amazon.Lambda;
using Amazon.Lambda.Model;
using Amazon.Runtime.CredentialManagement;

using System.Linq;

namespace EWSPushNotifications
{
    public partial class Main
    {
        private Session Session = null;

        public Listener Listener = null;

        private PushSubscription Subscription = null;

        private int NotificationFrequency = 1;

        private DateTime LastNotificationReceived = DateTime.MaxValue;

        private DateTime SubscriptionEstablished = DateTime.MinValue;

        private System.Timers.Timer Timer = null;

        public Main(string[] args)
        {
            Session = new Session(args);

            Timer = new System.Timers.Timer();
            Timer.Elapsed += new ElapsedEventHandler(Timer_Tick);

            Logger.InfoLogFile = Session.InfoLogFile;
            Logger.EventsLogFile = Session.EventsLogFile;
            Logger.ErrorLogFile = Session.ErrorLogFile;
            Logger.Verbose = Session.Verbose;
            Logger.DisableLogging = Session.DisableLogging;
            Logger.EnableEventLogging = Session.EnableEventLogging;

            Listener = new Listener();
            Listener.Port = Session.ListenPort;
            Listener.EventReceived += new Listener.ListenEventHandler(Listener_EventReceived);

            Console.WriteLine(Session.RegionEndpoint.ToString());
        }

        void Listener_EventReceived(object sender, ListenEventArgs a)
        {
            // We have received a notification
            LastNotificationReceived = DateTime.Now;

            UpdateEventTime();

            if (a.EventType == EventType.Status)
            {
                return;
            }

            string eventDetails = "";
            if (a.ItemId != null)
            {
                eventDetails = "Item " + a.EventType.ToString() + ": ";
            }
            else
            {
                eventDetails = "Folder " + a.EventType.ToString() + ": ";
            }

            try
            {
                ThreadPool.QueueUserWorkItem(new WaitCallback(DispatchItem), a);
                ThreadPool.QueueUserWorkItem(new WaitCallback(GetMoreInfo), a);
            }
            catch (Exception e)
            {
                Logger.LogError(e);
            }

            Logger.LogEvent(eventDetails);
        }

        public void DispatchItem(object a)
        {
            ListenEventArgs args = (ListenEventArgs)a;

            if (args.ItemId != null)
            {
                try
                {
                    var findStr = "@sms";
                    PropertySet propertySet = new PropertySet(BasePropertySet.FirstClassProperties);
                    EmailMessage emailMessage = EmailMessage.Bind(Session.ExchangeService, args.ItemId, propertySet);
                    
                    List<string> recipients = emailMessage.ToRecipients.Select(x => x.Address).ToList();
                    List<string> smsRecipients = recipients.Where(x => x.IndexOf(findStr) > -1).ToList();

                    List<string> CcRecipients = emailMessage.CcRecipients.Select(x => x.Address).ToList();
                    List<string> CcSmsRecipients = CcRecipients.Where(x => x.IndexOf(findStr) > -1).ToList();

                    if (smsRecipients.Count > 0 || CcSmsRecipients.Count > 0) {
                        SubscriptionEvent subscriptionEvent = new SubscriptionEvent()
                        {
                            ItemId = args.ItemId.ToString(),
                            Recipients = smsRecipients,
                            CcRecipients = CcSmsRecipients
                        };

                        MemoryStream payloadStream = new MemoryStream();
                        DataContractJsonSerializer serializer = new DataContractJsonSerializer(typeof(SubscriptionEvent));
                        serializer.WriteObject(payloadStream, subscriptionEvent);
                        string payload = Encoding.UTF8.GetString(payloadStream.ToArray());

                        Logger.LogMessage(payload, "Dispatching item event payload to AWS Lambda");


                        using (AmazonLambdaClient client = new AmazonLambdaClient(Session.RegionEndpoint))
                        {
                            InvokeRequest request = new InvokeRequest
                            {
                                FunctionName = Session.LambdaFunction,
                                Payload = payload
                            };

                            InvokeResponse response = client.Invoke(request);
                            using (StreamReader res = new StreamReader(response.Payload))
                            {
                                Logger.LogMessage(res.ReadToEnd(), "Item event payload dispatched to AWS Lambda.");
                            }
                        }
                    }
                }
                catch (Exception e)
                {
                    Logger.LogError(e, "Error dispatching item event payload to AWS Lambda.");
                }
            }
        }

        void GetMoreInfo(object a)
        {
            // Get more info for the given item.  This will run on it's own thread
            // so that the main program can continue as usual (we won't hold anything up)

            ListenEventArgs args = (ListenEventArgs)a;

            string eventDetails = "";
            if (args.ItemId != null)
            {
                eventDetails = "Item " + args.EventType.ToString() + ": ";
            }
            else
            {
                eventDetails = "Folder " + args.EventType.ToString() + ": ";
            }

            try
            {
                eventDetails += MoreInfo(args);
            }
            catch (Exception e)
            {
                Logger.LogError(e);
            }

            Logger.LogEvent(eventDetails);
        }

        private string MoreInfo(ListenEventArgs args)
        {
            // Find out some more information about the given item
            string info = "";
            if (args.ItemId != null && args.EventType != EventType.Deleted)
            {
                info += "Item (Id) subject=" + GetItemInfo(args.ItemId);
            }

            if (args.OldItemId != null && args.EventType != EventType.Deleted)
            {
                info += "Item (OldId) subject=" + GetItemInfo(args.ItemId);
            }

            if (args.FolderId != null)
            {
                if (!String.IsNullOrEmpty(info)) info += ", ";
                info += "Folder Name=" + GetFolderName(args.FolderId);
            }

            if (args.ParentFolderId != null)
            {
                if (!String.IsNullOrEmpty(info)) info += ", ";
                info += "Parent Folder Name=" + GetFolderName(args.ParentFolderId);
            }

            if (args.OldParentFolderId != null)
            {
                if (!String.IsNullOrEmpty(info)) info += ", ";
                info += "Old Parent Folder Name=" + GetFolderName(args.OldParentFolderId);
            }

            return info;
        }

        private string GetItemInfo(ItemId itemId)
        {
            // Retrieve the subject for a given item
            string itemInfo = "";
            Item item;
            PropertySet propertySet;
            propertySet = new PropertySet(BasePropertySet.FirstClassProperties, ItemSchema.MimeContent);

            try
            {
                item = Item.Bind(Session.ExchangeService, itemId, propertySet);
            }
            catch (Exception e)
            {
                Logger.LogError(e);
                return e.Message;
            }

            itemInfo += "Item subject=" + item.Subject;
            itemInfo += ", MIME length=" + item.MimeContent.Content.Length.ToString() + " bytes";

            return itemInfo;
        }

        private string GetFolderName(FolderId folderId)
        {
            // Retrieve display name of the given folder
            try
            {
                Folder folder = Folder.Bind(Session.ExchangeService, folderId, new PropertySet(FolderSchema.DisplayName));
                return folder.DisplayName;
            }
            catch (Exception e)
            {
                Logger.LogError(e);
                return e.Message;
            }
        }


        private void UpdateEventTime()
        {
            string lastNotificationReceived = "00:00:00";
            if (LastNotificationReceived != DateTime.MaxValue)
            {
                lastNotificationReceived = LastNotificationReceived.ToString("HH:mm:ss");
            }
            string info = "Last event received at: " + lastNotificationReceived;
            Logger.LogEvent(info);
        }

        public void Subscribe()
        {
            if (Session.ExchangeService == null)
            {
                return;
            }

            Subscription = null;
            try
            {
                if (String.IsNullOrEmpty(Session.WellKnownFolder))
                {
                    Subscription = Session.ExchangeService.SubscribeToPushNotificationsOnAllFolders(new Uri(Listener.ListenUri), NotificationFrequency, "", SelectedEvents());
                }
                else
                {
                    FolderId[] folders = new FolderId[1];
                    folders[0] = SubscribedFolderId();

                    Subscription = Session.ExchangeService.SubscribeToPushNotifications(folders, new Uri(Listener.ListenUri), NotificationFrequency, "", SelectedEvents());
                }
            }
            catch (Exception e)
            {
                Logger.LogError(e, "Failed to subscribe: EWS Url: " + Session.ExchangeService.Url);
                Session.ExchangeService = null;
                return;
            }

            if (Subscription == null)
            {
                Logger.LogMessage("Failed to subscribe.");
                return;
            }

            SubscriptionEstablished = DateTime.Now;
            Logger.LogMessage("Subscription successful.");
            Timer.Interval = (NotificationFrequency * 60000) + 10000;
            Timer.Start();
            Listener.SubscriptionId = Subscription.Id;
        }

        private FolderId SubscribedFolderId()
        {
            try
            {
                WellKnownFolderName wellKnownFolder = (WellKnownFolderName)Enum.Parse(typeof(WellKnownFolderName), Session.WellKnownFolder);

                return (!String.IsNullOrEmpty(Session.WellKnownFolder))
                    ? new FolderId(wellKnownFolder, new Mailbox(Session.Mailbox))
                    : wellKnownFolder;
            }
            catch (Exception e)
            {
                Logger.LogError(e);
                return null;
            }
        }

        private EventType[] SelectedEvents()
        {
            // Read the selected events

            if (Session.SelectedEvents.Length < 1)
            {
                return null;
            }

            EventType[] events = new EventType[Session.SelectedEvents.Length];

            for (int i = 0; i < Session.SelectedEvents.Length; i++)
            {
                switch (Session.SelectedEvents[i])
                {
                    case "NewMail": { events[i] = EventType.NewMail; break; }
                    case "Deleted": { events[i] = EventType.Deleted; break; }
                    case "Modified": { events[i] = EventType.Modified; break; }
                    case "Moved": { events[i] = EventType.Moved; break; }
                    case "Copied": { events[i] = EventType.Copied; break; }
                    case "Created": { events[i] = EventType.Created; break; }
                    case "FreeBusyChanged": { events[i] = EventType.FreeBusyChanged; break; }
                }
            }

            return events;
        }

        private void Timer_Tick(object sender, ElapsedEventArgs e)
        {
            TimeSpan maxRetry = new TimeSpan(0, NotificationFrequency * 6, 0);
            DateTime eventTimeout = DateTime.Now.Subtract(maxRetry);
            if (
                LastNotificationReceived > eventTimeout
                || (LastNotificationReceived == DateTime.MaxValue && SubscriptionEstablished > eventTimeout)
            )
            {
                // We've never received a notification, so check when we subscribed
                return;
            }

            // Subscription has timed out or otherwise failed
            Timer.Stop();

            string msg = String.Format("Attempting to resubscribe (last event received at {0})", LastNotificationReceived);
            Logger.LogMessage(msg, "Subscription timed out");
            Logger.LogMessage("Attempting to resubscribe");

            Subscribe();
            CatchUpOnMissedChanges(DateTime.Now, LastNotificationReceived);
        }

        private void CatchUpOnMissedChanges(DateTime before, DateTime after)
        {
            string msg = "";
            msg = String.Format("Looking for changes since {0}", after);
            Logger.LogMessage(msg, "Catching up on missed events.");

            FolderId monitoredFolderId = SubscribedFolderId();
            if (monitoredFolderId == null)
            {
                Logger.LogMessage("Only single folder is supported for event catch-up");
                return;
            }

            // We want to retrieve PR_LOCAL_COMMIT_TIME_MAX
            ExtendedPropertyDefinition PidTagLocalCommitTimeMax = new ExtendedPropertyDefinition(0x670A, MapiPropertyType.SystemTime);
            Folder monitoredFolder = Folder.Bind(Session.ExchangeService, monitoredFolderId, new PropertySet(BasePropertySet.IdOnly, FolderSchema.DisplayName, PidTagLocalCommitTimeMax));

            msg = String.Format("PidTagLocalCommitTimeMax = {0}; Looking for changes since {1}", monitoredFolder.ExtendedProperties[0].Value, after);
            Logger.LogMessage(msg);

            if ((DateTime)monitoredFolder.ExtendedProperties[0].Value <= after)
            {
                msg = String.Format("No missed changes detected in folder {0}", monitoredFolder.DisplayName);
                Logger.LogMessage(msg);
                return;
            }

            // Now we query all items in the folder modified between Changesafter and now
            ExtendedPropertyDefinition PidTagLastModificationTime = new ExtendedPropertyDefinition(0x3008, MapiPropertyType.SystemTime);
            ExtendedPropertyDefinition PidTagCreationTime = new ExtendedPropertyDefinition(0x3007, MapiPropertyType.SystemTime);
            int offset = 0;
            ItemView view = new ItemView(500, offset);
            view.PropertySet = new PropertySet(BasePropertySet.IdOnly, ItemSchema.Subject, PidTagLastModificationTime, PidTagCreationTime);

            SearchFilter.SearchFilterCollection searchCriteria = new SearchFilter.SearchFilterCollection(LogicalOperator.And);
            searchCriteria.Add(new SearchFilter.IsGreaterThan(PidTagLastModificationTime, after));
            searchCriteria.Add(new SearchFilter.IsLessThan(PidTagLastModificationTime, before));

            FindItemsResults<Item> results = monitoredFolder.FindItems(searchCriteria, view);
            if (results.TotalCount > 0)
            {
                msg = String.Format("{0} items found that have been modified since last notification", results.TotalCount);
                Logger.LogMessage(msg);
                foreach (Item item in results.Items)
                {
                    ListenEventArgs listenEventArgs = null;
                    DateTime itemCreatedTime = DateTime.MinValue;
                    DateTime itemModifiedTime = DateTime.MinValue;

                    foreach (ExtendedProperty prop in item.ExtendedProperties)
                    {
                        if (prop.PropertyDefinition.Equals(PidTagCreationTime))
                        {
                            itemCreatedTime = (DateTime)prop.Value;
                            Logger.LogMessage(itemCreatedTime.ToString(), "PidTagCreationTime");
                        }
                        else if (prop.PropertyDefinition.Equals(PidTagLastModificationTime))
                        {
                            itemModifiedTime = (DateTime)prop.Value;
                            Logger.LogMessage(itemModifiedTime.ToString(), "PidTagLastModificationTime");
                        }
                    }

                    try
                    {
                        if (itemCreatedTime >= after)
                        {
                            // This item is new
                            Logger.LogMessage("Item created since last notification was received");
                            listenEventArgs = new ListenEventArgs(Subscription.Id, EventType.Created, item.Id, monitoredFolder.Id);
                        }
                        else
                        {
                            Logger.LogMessage("Item created before last notification, so this is an update");
                            listenEventArgs = new ListenEventArgs(Subscription.Id, EventType.Modified, item.Id, monitoredFolder.Id);
                        }
                    }
                    catch { }
                    if (listenEventArgs != null)
                        Listener_EventReceived(null, listenEventArgs);
                }
            }
        }
    }
}