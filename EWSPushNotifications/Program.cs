using System;

namespace EWSPushNotifications
{
    public partial class Program
    {
        static void Main(string[] args)
        {
            Main main = new Main(args);
            main.Listener.StartListening();
            main.Subscribe();
        }
    }
}
