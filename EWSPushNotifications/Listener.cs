using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Net;
using System.IO;
using System.Reflection;
using Microsoft.Exchange.WebServices.Data;
using System.Xml;

namespace EWSPushNotifications
{
    public partial class Listener
    {

        private HttpListener HttpListener = null;

        public int Port = 36728;

        private string NotificationResponse = null;

        public string ListenUri = null;

        public string SubscriptionId = null;

        public delegate void ListenEventHandler(object sender, ListenEventArgs a);

        public event ListenEventHandler EventReceived;

        const string MessagesXsd = "http://schemas.microsoft.com/exchange/services/2006/messages";

        const string TypesXsd = "http://schemas.microsoft.com/exchange/services/2006/types";

        public int ListenPort
        {
            get
            {
                return Port;
            }
            set
            {
                if (value == Port) 
                {
                    return;
                }
                HttpListener.Stop();
                Port = value;
                StartListening();
            }
        }

        public Listener()
        {
            StreamReader reader = new StreamReader(Assembly.GetExecutingAssembly().GetManifestResourceStream("EWSPushNotifications.XML.EWSSendNotificationResult.xml"));
            NotificationResponse = reader.ReadToEnd();
            reader.Close();
            HttpListener = new HttpListener();

            string machineName = System.Net.Dns.GetHostEntry("LocalHost").HostName;
            ListenUri = "http://" + machineName + ":" + Port + "/" + GetType().Namespace + "/";
        }

        public void StartListening()
        {
            HttpListener.Prefixes.Clear();
            HttpListener.Prefixes.Add(ListenUri);

            try
            {
                HttpListener.Start();

                System.Threading.Thread listenerThread = new System.Threading.Thread(Listen);
                listenerThread.Start();
            }
            catch (Exception e)
            {
                Logger.LogError(e, "Unable to start HTTP listener (Are you running as Administrator?)");
            }
        }

        private void Listen(object state)
        {
            while(HttpListener.IsListening)
            {
                Logger.LogMessage("HTTP Listener listening: " + ListenUri);
                IAsyncResult context = HttpListener.BeginGetContext(new AsyncCallback(ListenerCallback), HttpListener);
                context.AsyncWaitHandle.WaitOne();
            }
        }

        protected virtual void NotificationReceived(ListenEventArgs e)
        {
            ListenEventHandler handler = EventReceived;

            if (handler != null)
            {
                handler(this, e);
            }
        }

        public void ListenerCallback(IAsyncResult result)
        {
            try
            {
                HttpListener listener = (HttpListener)result.AsyncState;
                HttpListenerContext context = listener.EndGetContext(result);
                HttpListenerRequest request = context.Request;
                string req = "";

                using (StreamReader reader = new StreamReader(request.InputStream))
                {
                    req = reader.ReadToEnd();
                    Logger.LogMessage(req, "Notification");
                }

                DetermineResponse(req, context.Response);
                HttpListener.BeginGetContext(new AsyncCallback(ListenerCallback), HttpListener);
            }
            catch (Exception e)
            {
                Logger.LogError(e);
            }
        }

        private void DetermineResponse(string request, HttpListenerResponse response)
        {
            if (request.Contains("exchange") && request.Contains("SendNotificationResponseMessage"))
            {
                // This is an EWS notification message
                WriteEWSSubscriptionResponse(response);
                RaiseEWSEvents(request);
            }
            response.OutputStream.Flush();
            response.Close();
        }

        private void WriteEWSSubscriptionResponse(HttpListenerResponse response)
        {
            // Send EWS OK response
            byte[] buffer = System.Text.Encoding.UTF8.GetBytes(NotificationResponse);
            response.ContentLength64 = buffer.Length;
            response.ContentType = "text/xml";
            using (Stream output = response.OutputStream)
            {
                output.Write(buffer, 0, buffer.Length);
            }
            Logger.LogMessage(NotificationResponse, "Notification Response");
        }

        private void RaiseEWSEvents(string request)
        {
            // Process the notifications
            XmlDocument notifications = new XmlDocument();
            try
            {
                notifications.LoadXml(request);
            }
            catch (Exception e)
            {
                Logger.LogError(e, "Failed to load notifications XML.");
                return;
            }

            XmlNodeList nodeListNotifications = notifications.GetElementsByTagName("Notification", MessagesXsd);
            if (nodeListNotifications.Count == 0) return;

            // Extract the subscriptionId
            XmlNode nodeSubscriptionId = notifications.GetElementsByTagName("SubscriptionId", TypesXsd)[0];
            string subscriptionId = nodeSubscriptionId.InnerText;

            // Check whether this is for this subscription
            if (!String.IsNullOrEmpty(SubscriptionId) && subscriptionId != SubscriptionId)
            {
                return;
            }

            // Now process the events
            foreach (XmlNode node in nodeListNotifications[0].ChildNodes)
            {
                switch (node.LocalName)
                {
                    case "StatusEvent":
                        NotificationReceived(new ListenEventArgs(subscriptionId,
                        EventType.Status));
                        break;

                    case "NewMailEvent":
                        NotificationReceived(new ListenEventArgs(subscriptionId,
                        EventType.NewMail,
                        ExtractItemId("ItemId", node), ExtractFolderId("ParentFolderId", node)));
                        break;

                    case "CreatedEvent":
                        if (node.InnerXml.Contains("ItemId"))
                        {
                            NotificationReceived(new ListenEventArgs(subscriptionId,
                                EventType.Created,
                                ExtractItemId("ItemId", node), ExtractFolderId("ParentFolderId", node)));
                        }
                        else
                        {
                            NotificationReceived(new ListenEventArgs(subscriptionId,
                                EventType.Created,
                                ExtractFolderId("FolderId", node), ExtractFolderId("ParentFolderId", node)));
                        }
                        break;

                    case "MovedEvent":
                        if (node.InnerXml.Contains("ItemId"))
                        {
                            NotificationReceived(new ListenEventArgs(subscriptionId,
                                EventType.Moved,
                                ExtractItemId("ItemId", node), ExtractFolderId("ParentFolderId", node),
                                ExtractItemId("OldItemId", node), ExtractFolderId("OldParentFolderId", node)));
                        }
                        else
                        {
                            NotificationReceived(new ListenEventArgs(subscriptionId,
                                EventType.Moved,
                                ExtractFolderId("FolderId", node), ExtractFolderId("ParentFolderId", node),
                                ExtractFolderId("OldFolderId", node), ExtractFolderId("OldParentFolderId", node)));
                        }
                        break;

                    case "CopiedEvent":
                        if (node.InnerXml.Contains("ItemId"))
                        {
                            NotificationReceived(new ListenEventArgs(subscriptionId,
                                EventType.Copied,
                                ExtractItemId("ItemId", node), ExtractFolderId("ParentFolderId", node),
                                ExtractItemId("OldItemId", node), ExtractFolderId("OldParentFolderId", node)));
                        }
                        else
                        {
                            NotificationReceived(new ListenEventArgs(subscriptionId,
                                EventType.Copied,
                                ExtractFolderId("FolderId", node), ExtractFolderId("ParentFolderId", node),
                                ExtractFolderId("OldFolderId", node), ExtractFolderId("OldParentFolderId", node)));
                        }
                        break;

                    case "DeletedEvent":
                        if (node.InnerXml.Contains("ItemId"))
                        {
                            NotificationReceived(new ListenEventArgs(subscriptionId,
                                EventType.Deleted,
                                ExtractItemId("ItemId", node), ExtractFolderId("ParentFolderId", node),
                                ExtractItemId("OldItemId", node), ExtractFolderId("OldParentFolderId", node)));
                        }
                        else
                        {
                            NotificationReceived(new ListenEventArgs(subscriptionId,
                                EventType.Deleted,
                                ExtractFolderId("FolderId", node), ExtractFolderId("ParentFolderId", node),
                                ExtractFolderId("OldFolderId", node), ExtractFolderId("OldParentFolderId", node)));
                        }
                        break;

                    case "ModifiedEvent":
                        if (node.InnerXml.Contains("ItemId"))
                        {
                            NotificationReceived(new ListenEventArgs(subscriptionId,
                                EventType.Modified,
                                ExtractItemId("ItemId", node), ExtractFolderId("ParentFolderId", node),
                                ExtractItemId("OldItemId", node), ExtractFolderId("OldParentFolderId", node)));
                        }
                        else
                        {
                            NotificationReceived(new ListenEventArgs(subscriptionId,
                                EventType.Modified,
                                ExtractFolderId("FolderId", node), ExtractFolderId("ParentFolderId", node),
                                ExtractFolderId("OldFolderId", node), ExtractFolderId("OldParentFolderId", node)));
                        }
                        break;
                }
            }
        }

        private FolderId ExtractFolderId(string tag, XmlNode rootNode)
        {
            // Extract a FolderId from the node
            string folderId = "";
            string changeKey = "";

            foreach (XmlNode node in rootNode.ChildNodes)
            {
                if (node.LocalName == tag)
                {
                    // This is our ItemId
                    folderId = node.Attributes["Id"].Value;
                    changeKey = node.Attributes["ChangeKey"].Value;
                    break;
                }
            }
            if (!String.IsNullOrEmpty(folderId))
            {
                FolderId id = new FolderId(folderId);
                return id;
            }
            return null;
        }

        private ItemId ExtractItemId(string tag, XmlNode rootNode)
        {
            // Extract an ItemId from the node
            string itemId = "";
            string changeKey = "";

            foreach (XmlNode node in rootNode.ChildNodes)
            {
                if (node.LocalName == tag)
                {
                    // This is our ItemId
                    itemId = node.Attributes["Id"].Value;
                    changeKey = node.Attributes["ChangeKey"].Value;
                    break;
                }
            }
            if (!String.IsNullOrEmpty(itemId))
            {
                ItemId id = new ItemId(itemId);
                return id;
            }
            return null;
        }
    }

    public class ListenEventArgs : EventArgs
    {
        private string Xml = "";
        
        private string SubscriptionId = "";
        
        public EventType EventType;
        
        public ItemId ItemId = null;
        
        public FolderId FolderId = null;
        
        public FolderId ParentFolderId = null;
        
        public ItemId OldItemId = null;
        
        public FolderId OldFolderId = null;
        
        public FolderId OldParentFolderId = null;

        public ListenEventArgs(string XML)
        {
            // Store the XML, then parse it to extract the notification details
            Xml = XML;

        }

        public ListenEventArgs(string subscriptionId, EventType eventType)
        {
            SubscriptionId = subscriptionId;
            EventType = eventType;
        }

        public ListenEventArgs(string subscriptionId, EventType eventType, ItemId itemId, FolderId parentFolderId)
        {
            SubscriptionId = subscriptionId;
            EventType = eventType;
            ItemId = itemId;
            ParentFolderId = parentFolderId;
        }

        public ListenEventArgs(string subscriptionId, EventType eventType, ItemId itemId, FolderId parentFolderId, ItemId oldItemId, FolderId oldParentFolderId)
            : this(subscriptionId, eventType, itemId, parentFolderId)
        {
            OldItemId = oldItemId;
            OldParentFolderId = oldParentFolderId;
        }

        public ListenEventArgs(string subscriptionId, EventType eventType, FolderId folderId, FolderId parentFolderId)
        {
            SubscriptionId = subscriptionId;
            EventType = eventType;
            FolderId = folderId;
            ParentFolderId = parentFolderId;
        }

        public ListenEventArgs(string subscriptionId, EventType eventType, FolderId folderId, FolderId parentFolderId, FolderId oldFolderId, FolderId oldParentFolderId)
            : this(subscriptionId, eventType, folderId, parentFolderId)
        {
            OldFolderId = oldFolderId;
            OldParentFolderId = oldParentFolderId;
        }
    }
}