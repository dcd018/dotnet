using System;
using System.Text;
using System.Collections.Generic;
using System.Collections.ObjectModel;

using System.Management.Automation;
using System.Management.Automation.Runspaces;
using System.Management.Automation.Tracing;
using System.Security;

using CommandLine;

namespace ExchangeManagementConsole
{
    class Options
    {
        [Option(Required = true, HelpText = "Exchange server address.")]
        public string Host { get; set; }

        [Option(Required = true, HelpText = "Exchange server domain.")]
        public string Domain { get; set; }

        [Option(Required = true, HelpText = "Exchange user with administrative priviledges.")]
        public string User { get; set; }

        [Option(Required = true, HelpText = "Password of Exchange user with administrative priviledges.")]
        public string Password { get; set; }

        [Option(Required = true, HelpText = "The command to run.")]
        public string Cmd { get; set; }
    }

    partial class Main
    {
        private WSManConnectionInfo ConnectionInfo = null;

        public Main(string[] args)
        {
            ParserResult<Options> parsed = Parser.Default.ParseArguments<Options>(args);
            parsed.WithParsed(options =>
            {
                try
                {
                    WSManConnectionInfo connectionInfo = getConnectionInfo(options.Host, options.Domain, options.User, options.Password);
                    string result = CreateRunspace(connectionInfo, options.Cmd);
                    Console.WriteLine(result);
                }
                catch(Exception e)
                {
                    Console.WriteLine("An error occurred.");
                    Console.WriteLine(e.Message);
                }
            });
        }

        public SecureString GetSecureString(string password)
        {
            if (password == null)
            {
                throw new ArgumentNullException("password");
            }
            SecureString securePassword = new SecureString();
            foreach (char c in password)
            {
                securePassword.AppendChar(c);
            }
            securePassword.MakeReadOnly();
            return securePassword;
        }

        private WSManConnectionInfo getConnectionInfo(string host, string domain, string user, string password)
        {
            PSCredential credentials = new PSCredential(string.Format("{0}\\{1}", domain, user), GetSecureString(password));
            Uri uri = new Uri(string.Format("https://{0}/powershell", host));
            WSManConnectionInfo connectionInfo = new WSManConnectionInfo(
                uri,
                "http://schemas.microsoft.com/powershell/Microsoft.Exchange",
                credentials
            );
            connectionInfo.AuthenticationMechanism = AuthenticationMechanism.Basic;
            connectionInfo.SkipCACheck = true;
            connectionInfo.SkipCNCheck = true;
            return connectionInfo;
        }

        private string CreateRunspace(WSManConnectionInfo connectionInfo, string command)
        {
            using (Runspace runspace = RunspaceFactory.CreateRunspace(connectionInfo))
            {
                runspace.Open();
                return RunCommand(runspace, command);
            }
        }

        public string RunCommand(Runspace runspace, string command)
        {
            using (Pipeline pipeline = runspace.CreatePipeline())
            {
                pipeline.Commands.AddScript(command);
                Collection<PSObject> result = pipeline.Invoke();
                runspace.Close();
                if (pipeline.Error != null && pipeline.Error.Count > 0)
                {
                    StringBuilder exception = new StringBuilder();
                    var error = pipeline.Error.Read() as Collection<ErrorRecord>;
                    if (error != null) 
                    {
                        foreach (ErrorRecord err in error)
                        {
                            exception.Append(err.ToString());
                        }
                    }
                    throw new Exception(exception.ToString());
                } 
                else 
                {
                    StringBuilder response = new StringBuilder();
                    foreach (PSObject obj in result)
                    {
                        response.Append(obj.ToString());
                    }
                    runspace.Dispose();
                    pipeline.Dispose();
                    return response.ToString();
                }
            }
        }
    }
}
